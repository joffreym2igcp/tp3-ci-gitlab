# Documentation du pipeline CI/CD pour une application Python

Ce document décrit le pipeline CI/CD pour une application Python. Il explique les étapes du pipeline, les fichiers nécessaires et la configuration des paramètres.

## Étapes du pipeline

Le pipeline CI/CD est composé des étapes suivantes :

1. Récupération du code source : le code source de l'application est récupéré à partir d'un dépôt Git.
2. Linting : le code source est vérifié pour s'assurer qu'il respecte les normes de codage.
3. Vérification des copier-coller : le code source est vérifié pour détecter les copier-coller.
4. Analyse de la complexité cyclomatique : la complexité cyclomatique du code source est analysée.
5. Tests unitaires : les tests unitaires de l'application sont exécutés.
6. Construction de l'image Docker : une image Docker de l'application est construite.
7. Push de l'image Docker sur Docker Hub : l'image Docker construite est poussée sur Docker Hub.

## Fichiers nécessaires

Les fichiers suivants sont nécessaires pour le pipeline CI/CD :

- `.gitlab-ci.yml` : ce fichier définit les étapes du pipeline CI/CD.
- `Dockerfile` : ce fichier définit la construction de l'image Docker de l'application.
- `docker-compose.yml` : ce fichier définit les services nécessaires pour exécuter le pipeline CI/CD.

``` sh
.
├── README.md
├── app
│   ├── application
│   │   ├── __init__.py
│   │   └── application.py
│   ├── command
│   │   ├── __init__.py
│   │   └── command.py
│   ├── machine
│   │   ├── __init__.py
│   │   ├── machine.py
│   │   └── machines.json
│   ├── main.py
│   ├── requirements.txt
│   └── test
│       ├── __init__.py
│       ├── system
│       │   └── machine.robot
│       └── unit
│           ├── __init__.py
│           └── test.py
├── docker-app
│   └── python
│       └── Dockerfile
├── docker-compose.yml
├── pipeline-ci-schema
│   └── pipeline-ci.jpg
└── tp_3_CI.pdf
```

11 directories, 18 files

## Configuration des paramètres

Les paramètres du pipeline CI/CD peuvent être configurés en définissant des variables dans le fichier `.gitlab-ci.yml`. Les variables suivantes sont disponibles :

- `GIT_URL` : l'URL du dépôt Git.
- `GIT_USERNAME` et `GIT_PASSWORD` : les identifiants pour accéder au dépôt Git.
- `PYLINT_CONFIG_PATH`, `RADON_RAW_CONFIG_PATH`, `RADON_CC_CONFIG_PATH` et `UNITTEST_CONFIG_PATH` : les chemins vers les fichiers de configuration des outils d'analyse qualité.
- `DOCKERFILE_PATH` et `DOCKER_CONTEXT` : le chemin vers le Dockerfile et le contexte du build Docker.
- `DOCKER_IMAGE_NAME` et `DOCKER_IMAGE_TAG

Stages (Étapes) :

Le pipeline est composé de deux étapes : test (test) et deploy (déploiement) :

1. pylint-test (test de pylint) :

    - Cette tâche installe les dépendances Python nécessaires.
    Elle exécute pylint pour analyser le code Python et génère des rapports en JSON et en HTML.
    - Les rapports générés sont archivés comme artefacts.

2. unittest-test (test unittest) :

    - Cette tâche installe également les dépendances Python nécessaires.
    - Elle exécute les tests unitaires à l'aide de unittest avec le fichier spécifié dans la variable $UNITTEST_FILE.
    - Les rapports de test sont archivés comme artefacts.

3. radon-raw-test (test radon raw) :

    - Cette tâche installe des dépendances, y compris Radon pour l'analyse de la complexité du code.
    - Elle exécute Radon pour générer des rapports sur la complexité du code en JSON et en HTML.
    - Les rapports générés sont archivés comme artefacts.

4. radon-cc-test (test radon cc) :

    - Cette tâche est similaire à radon-raw-test, mais elle analyse la complexité cyclomatique du code.
    - Les rapports générés sont archivés comme artefacts.

5. build-image (construction de l'image Docker) :

    - Cette tâche est dans l'étape de déploiement (deploy) et utilise l'image Docker.
    - Elle se connecte à Docker Hub avec les informations d'identification spécifiées.
    - Elle construit une image Docker à partir du fichier Dockerfile spécifié.
    - L'image Docker est ensuite poussée vers Docker Hub.

Globalement, ce fichier .gitlab-ci.yml définit un pipeline de CI/CD qui effectue divers tests (linting, unittest, analyse de la complexité du code) et déploie une image Docker en fonction des résultats des tests.

Le fichier Dockerfile permet de construire une image Docker qui exécute une application Flask. Voici une brève analyse de ce Dockerfile :

1. Vous utilisez l'image de base officielle de Docker, python:3, qui contient Python 3.

2. Le répertoire de travail dans le conteneur est défini comme /app en utilisant la commande WORKDIR.

3. Tous les fichiers et répertoires du répertoire app/ de votre système hôte sont copiés dans le répertoire de travail du conteneur Docker à l'aide de la commande COPY.

4. Les dépendances Python spécifiées dans le fichier requirements.txt sont installées dans le conteneur Docker à l'aide de la commande pip install. L'option --no-cache-dir est utilisée pour garantir que les dépendances sont toujours installées à partir de la source plutôt que d'utiliser un cache local.

5. Vous définissez une variable d'environnement FLASK_APP avec la valeur "main". Cette variable est utilisée par l'application Flask pour spécifier le module principal de l'application.

6. Enfin, la commande CMD spécifie la commande qui sera exécutée lorsque le conteneur Docker est démarré. Dans ce cas, vous démarrez l'application Flask en utilisant python3 -m flask run --host=0.0.0.0, ce qui permet à l'application d'écouter sur toutes les interfaces réseau disponibles.

Le fichier docker-compose.yml définit les services nécessaires pour exécuter le pipeline CI/CD. Il définit deux services : app et docker-app. Le service app est utilisé pour exécuter le pipeline CI/CD. Le service docker-app est utilisé pour construire l'image Docker de l'application. Le service docker-app utilise le fichier Dockerfile dans le répertoire docker-app/python pour construire l'image Docker.

## Exécution du pipeline CI/CD

Pour exécuter ce pipeline sur GitLab, vous devez suivre ces étapes :

Créez un nouveau fichier dans votre dépôt GitLab et nommez-le .gitlab-ci.yml. GitLab utilise ce fichier pour configurer le pipeline d’intégration continue.

Copiez et collez le contenu de votre pipeline dans ce fichier. Assurez-vous que le contenu est correctement formaté en YAML.

Ajoutez et validez le fichier .gitlab-ci.yml à votre dépôt. GitLab commencera à exécuter votre pipeline d’intégration continue chaque fois que vous poussez des modifications sur votre dépôt.

Pour voir l’état de votre pipeline, allez dans votre projet sur GitLab, cliquez sur CI/CD dans le menu de gauche, puis cliquez sur Pipelines.

Si vous avez défini des variables d’environnement dans votre pipeline (comme $DOCKER_USER, $DOCKER_REPO et $DOCKER_IMAGE_VERSION), vous devez les ajouter à votre projet GitLab. Pour ce faire, allez dans votre projet sur GitLab, cliquez sur Settings (Paramètres) dans le menu de gauche, puis cliquez sur CI/CD. Déroulez la section Variables, ajoutez vos variables et leurs valeurs, puis cliquez sur Add Variable (Ajouter une variable).
